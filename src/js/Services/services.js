'use strict';

/* Services */

var ttServices = angular.module('ttServices', ['firebase']);

ttServices.factory('games', function (db, $firebaseArray) {
    var data = $firebaseArray(db);

    return {
        getGames: function () {
            return data.$loaded();
        },
        getGame: function (id) {
            return this.getGames().then(function (games) {
                    return games.$getRecord(id);
                }
            )
        },

        getPlayers: function () {
            return this.getGames().then(function (games) {
                    return _.map(_.uniq(games, 'opponent'), function (el) {
                        return el.opponent;
                    })
            })
        },

        addGame: function (game) {
            game.date = moment().unix();
            data.$add(game);
        },

        removeGame: function (game) {
            data.$remove(game);
        },

        updateGame: function () {
            data.$save();
        }
    };
});

ttServices.service('db', function () {
    return new Firebase("https://sweltering-inferno-4095.firebaseio.com/games");
});
