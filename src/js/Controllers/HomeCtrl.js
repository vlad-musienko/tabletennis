angular.module('home', ['scroll', 'chart.js'])
    .controller('HomeCtrl', function ($scope, games, data, db) {
        $scope.games = data || [];
        $scope.pageSize = 0;

        $scope.games.$watch(function () {
            $scope.getMonthStats();
        });


        $scope.loadMore = function () {
            if ($scope.pageSize <= $scope.games.length) {
                $scope.pageSize += 10;
            }
        };

        $scope.getGamesCount = function () {
            return $scope.games.length;
        };

        $scope.getWins = function () {
            return $scope.games.filter(function (game) {
                return game.myScore > game.oppScore;
            }).length;
        };

        $scope.getWinsPercent = function () {
            return ($scope.getWins() / $scope.games.length * 100).toFixed(2) / 1;
        };

        $scope.getLoses = function () {
            return $scope.getGamesCount() - $scope.getWins();
        };

        $scope.getLosesPercent = function () {
            return 100 - $scope.getWinsPercent().toFixed(2)
        };

        //Stats for drawing chart per month
        $scope.getMonthStats = function () {
            var monthStart = moment().startOf('month').unix();
            var monthEnd = moment().endOf('month').unix();
            var results = {},
                wins = [],
                loses = [],
                labels,
                i;
            db.orderByChild("date")
                .on("value", function (snapshot) {
                    console.log(snapshot.val());
                    $scope.gamesPerMonth = snapshot.val();
                });

            angular.forEach($scope.gamesPerMonth, function (game) {
                //Current day
                var day = moment.unix(game.date).format("DD");
                //Check if exist or create object
                results[day] = results[day] || {
                    wins: 0,
                    loses: 0,
                    total: 0
                };
                //Calc stats
                if (game.oppScore > game.myScore) {
                    results[day].loses += 1;
                } else {
                    results[day].wins += 1;
                }
            });

            labels = Object.keys(results);
            labels.sort(function (a, b) {
                return a - b
            });
            for (i = 0; i < labels.length; i++) {
                wins[i] = results[labels[i]].wins;
                loses[i] = results[labels[i]].loses;
            }

            //Chart data
            $scope.labels = labels;
            $scope.series = ['Wins', 'Loses'];
            $scope.data = [wins, loses];
            $scope.colours = ['#5cb85c', '#ee5f4c'];
        };


        $scope.getMonthStats();
        $scope.loadMore();
    }
)
;