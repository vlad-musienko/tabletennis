angular.module('play', [])
    .controller('PlayCtrl', function ($scope, games, $state) {
        //First serve 1 - i`m , 0 - opponent firstServe
        $scope.currentServe = 0;
        $scope.myScore = 0;
        $scope.oppScore = 0;
        $scope.firstServer = 0;
        $scope.gameStarted = false;

        games.getPlayers().then(function(players){
            $scope.players = players;
        });

        $scope.start = function(){
            $scope.gameStarted = true;
            $scope.formHide = true;
            $scope.server = $scope.firstServer;
        };

        $scope.oppGoal = function(){
            $scope.oppScore++;
        };

        $scope.myGoal = function(){
            $scope.myScore++;
        };

        $scope.$watchGroup(['myScore','oppScore'], function(scores){
            var myScores = scores[0];
            var oppScores = scores[1];

            //Tracking who serve
            if($scope.currentServe % $scope.serves == 0 && $scope.gameStarted){
                $scope.server = !$scope.server * 1;

            }
            //Tracking end game
            if((myScores || oppScores) == $scope.gameTo){
                games.addGame({
                    opponent: $scope.opponent,
                    oppScore: oppScores,
                    myScore: myScores
                });
                var winner = myScores == $scope.gameTo ? "I`m winner!": "Opponent win! :(";
                alert(winner, "Game saved into game list");
                $state.go('home');
            }
            $scope.currentServe++;
        })
    });