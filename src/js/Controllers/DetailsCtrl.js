angular.module('details', ['angularUtils.directives.dirPagination'])
    .controller('DetailsCtrl', function ($scope, games, data) {
        $scope.games = data || [];
        $scope.pageSize = 20;

        $scope.removeGame = function (game) {
            games.removeGame(game);
        };
    });