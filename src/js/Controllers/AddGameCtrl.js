angular.module('addGame', ['Mac'])
    .controller('AddGameCtrl', function ($scope, games, $state) {

        games.getPlayers().then(function(players){
            $scope.players = players;
        });

        $scope.addGame = function () {
            games.addGame({
                opponent: $scope.opponent,
                oppScore: $scope.oppScore,
                myScore: $scope.myScore
            });

            $scope.opponent = "";
            $scope.oppScore = "";
            $scope.myScore = "";
            alert("game successfully added");
            $state.go('home');
        };
    });