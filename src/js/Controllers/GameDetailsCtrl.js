angular.module('gameDetails', [])
    .controller('GameDetailsCtrl', function ($scope, games, $stateParams, $state) {
        games.getGame($stateParams.id).then(function (game) {
            $scope.game = game;
        }).then(function () {
            games.getGames().then(function (games) {
                $scope.gamesVsPlayer = _.where(games, {opponent: $scope.game.opponent});

            });
        });
    });