angular.module('top', [])
    .controller('TopCtrl', function ($scope, games) {
        $scope.stats = {};
        $scope.top = [];


        //TODO optimization this code
        games.getGames().then(function (allGames) {
            games.getPlayers().then(function (players) {
                $scope.stats["My Rank"] = {
                    total: 0,
                    wins: 0
                };
                //Find all player games
                angular.forEach(players, function (player) {
                    var playerGames = _.where(allGames, {opponent: player});
                    //Calculate wins and total games
                    angular.forEach((playerGames), function (game) {
                        $scope.stats[player] = $scope.stats[player] || {
                            total: 0,
                            wins: 0
                        };
                        $scope.stats[player].total += 1;
                        $scope.stats["My Rank"].total += 1;


                        if (game.oppScore > game.myScore) {
                            $scope.stats[player].wins += 1;
                        } else {
                            $scope.stats["My Rank"].wins += 1;
                        }
                    });
                });
                //Calc wins percentage
                angular.forEach($scope.stats, function(playerScores, key){
                    var percentage = (((playerScores.wins / playerScores.total) * 100).toFixed(2)) / 1;
                    $scope.top.push({name:key, percentage: percentage})
                });
                //Sorting results
                $scope.top =_.sortBy($scope.top, 'percentage');

            });
        })
    });
