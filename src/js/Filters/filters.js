'use strict';
angular.module('ttFilters', [])
    .filter('unixTimestamp', function () {
        return function (timestamp) {
            if (typeof  timestamp != "number") return;
            return moment.unix(timestamp).format("DD/MM/YYYY");
        };
    })
    .filter('whoServing', function () {
        return function (bool) {
            if (bool == 0) {
                return "You serve"
            } else {
                return "Opponent Serve";
            }
        };
    });
