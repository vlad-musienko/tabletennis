'use strict';

/* App Module */

var tableTennis = angular.module('tableTennis', ['ui.router', 'ttServices', 'ttControllers', 'ttFilters',
    'ttRouting', 'firebase']);

tableTennis.config(function(paginationTemplateProvider) {
    paginationTemplateProvider.setPath('templates/dirPagination.tpl.html');
});