'use strict';

/* ui-routing */
var ttRounting = angular.module('ttRouting', ['ui.router', 'ttControllers'])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $urlRouterProvider.otherwise("/");

        $stateProvider
            .state('home', {
                url: "/",
                templateUrl: "templates/home.html",
                controller: 'HomeCtrl',
                resolve: {
                    data: ['games',
                        function (games) {
                            return games.getGames();
                        }]
                }
            })
            .state('add', {
                url: "/add",
                templateUrl: "templates/add.html",
                controller: "AddGameCtrl"
            })
            .state('details', {
                url: "/details",
                templateUrl: "templates/details.html",
                controller: "DetailsCtrl",
                resolve: {
                    data: ['games',
                        function (games) {
                            return games.getGames();
                        }]
                }
            })
            .state('gameDetails', {
                url: "/game/:id",
                templateUrl: "templates/gameDetails.html",
                controller: "GameDetailsCtrl"
            })
            .state('top', {
                url: "/top",
                templateUrl: "templates/playersTop.html",
                controller: "TopCtrl"
            })
            .state('play', {
                url: "/play",
                templateUrl: "templates/playGame.html",
                controller: "PlayCtrl"
            })
    });