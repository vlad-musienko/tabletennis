/**
 * Created by vlad on 13.04.15.
 */
var gulp = require('gulp'),
    ngAnnotate = require('gulp-ng-annotate'),
    uglify = require('gulp-uglify'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    uglifycss = require('gulp-uglifycss');

gulp.task('default', ['clean', 'vendors-js', 'angular-app-js', 'vendors-css'], function () {
});

gulp.task('vendors-js', function () {
    gulp.src([
        'vendors/jquery/dist/jquery.js',
        'vendors/angular/angular.js',
        'vendors/angular/dist/angular-chart.js',
        'vendors/angular-macgyver/lib/macgyver.js',
        'vendors/angular-ui-router/release/angular-ui-router.js',
        'vendors/angular-utils-pagination/dirPagination.js',
        'vendors/angularfire/dist/angularfire.js',
        'vendors/bootstrap/dist/js/bootstrap.js',
        'vendors/Chart.js/Chart.js',
        'vendors/angular-chart.js/dist/angular-chart.js',
        'vendors/firebase/firebase.js',
        'vendors/moment/moment.js',
        'vendors/underscore/underscore.js'
    ])
        .pipe(concat('vendors-js.min.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest('app/js'))
        .pipe(notify("Vendors js compiled"))
});

gulp.task('vendors-css', function () {
    gulp.src([
        'vendors/bootstrap/dist/css/bootstrap.css',
        'vendors/angular-chart.js/dist/angular-chart.css',
        'vendors/angular-macgyver/lib/macgyver.css'
    ])
        .pipe(concat('vendors-css.min.css'))
        .pipe(uglifycss())
        .pipe(gulp.dest('app/css/'))
        .pipe(notify("Vendors css compiled"))
});


gulp.task('angular-app-js', function () {
    gulp.src('src/js/**/*.js')
        .pipe(concat('angular-app.min.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest('app/js/'))
        .pipe(notify("App compiled!"));
});

gulp.task('clean', function () {
    gulp.src(['app/css/vendors-css.min.css', 'app/js/*'])
        .pipe(clean());
});

gulp.task('watch', function () {
        gulp.watch('src/js/**/*.js', ['angular-app-js']);
});