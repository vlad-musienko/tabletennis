module.exports = function(config){
  config.set({

    basePath : '../',

    files : [
      '../app/vendors/angular/angular.js',
      '../app/js/**/*.js',
      '/unit/**/*.js'
    ],

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],

    plugins : [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine'
            ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};